import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import WishlistItem from "../components/Wishlistitem";

test("renders wishlist item with correct title and description", () => {
  const item = {
    id: 1,
    title: "Trip 1",
    description: "Description 1",
    price: 100,
    startTrip: new Date(),
    endTrip: new Date(),
  };
  const removeFromWishlist = jest.fn();
  render(<WishlistItem item={item} removeFromWishlist={removeFromWishlist} />);
  const titleElement = screen.getByText("Trip 1");
  const descriptionElement = screen.getByText("Description 1");
  expect(titleElement).toBeInTheDocument();
  expect(descriptionElement).toBeInTheDocument();
});

test("calls removeFromWishlist function when 'delete Item' button is clicked", () => {
  const item = {
    id: 1,
    title: "Trip 1",
    description: "Description 1",
    price: 100,
    startTrip: new Date(),
    endTrip: new Date(),
  };
  const removeFromWishlist = jest.fn();
  render(<WishlistItem item={item} removeFromWishlist={removeFromWishlist} />);
  const deleteButton = screen.getByText("delete Item");
  fireEvent.click(deleteButton);
  expect(removeFromWishlist).toHaveBeenCalledWith(item);
});