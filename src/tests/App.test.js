import {logRoles, render, screen} from "@testing-library/react";
import App from "../App";


test('App renders a heading', () => {
  render(<App />)

  screen.getByRole('heading', {
    name: "Welcome to biztrips Happy new Year-react - 2024",
  })

});


