import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TripList from '../components/TripList';

test('Renders TripList component', () => {
    render(<TripList addToWishlist={() => {}} />);
    // Überprüfen, ob die Triplist-Catalog-Überschrift gerendert wird
    const catalogHeader = screen.getByText('Triplist-Catalog');
    expect(catalogHeader).toBeInTheDocument();
    // Überprüfen, ob der "Filter by Month" Label gerendert wird
    const filterLabel = screen.getByText('Filter by Month:');
    expect(filterLabel).toBeInTheDocument();
    // Überprüfen, ob die "All Months" Option im Select gerendert wird
    const allMonthsOption = screen.getByText('All Months');
    expect(allMonthsOption).toBeInTheDocument();
    // Überprüfen, ob die "Productlist is empty" Meldung gerendert wird (da die Triplist initial leer ist)
    const emptyMessage = screen.getByText('Productlist is empty');
    expect(emptyMessage).toBeInTheDocument();
  });