import { render, screen , fireEvent} from '@testing-library/react';
import Trip from '../components/Trip';



test('renders trip component', () => {
    const trip = {
      id: 1,
      title: 'Test Trip',
      description: 'This is a test trip',
      startTrip: [2022, 10, 1],
      endTrip: [2022, 10, 10]
    };
  
    const addToWishlist = jest.fn(); // Mock the addToWishlist function
  
    render(<Trip trip={trip} addToWishlist={addToWishlist} />);
  
    const titleElement = screen.getByRole('heading', { name: /Test Trip/i });
    const descriptionElement = screen.getByText(/This is a test trip/i);
    expect(titleElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
  
    const addToWishlistButton = screen.getByRole('button', { name: /Add to Wishlist/i });
    fireEvent.click(addToWishlistButton);
    expect(addToWishlist).toHaveBeenCalledWith(trip); // Verify that addToWishlist was called with the correct trip
  });