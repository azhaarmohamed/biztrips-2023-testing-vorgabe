import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Wishlist from "../components/Wishlist";
import WishlistItem from "../components/Wishlistitem";

test("renders empty wishlist message when wishlist is empty", () => {
  const wishlist = [];
  const removeFromWishlist = jest.fn();
  const clearWishlist = jest.fn();
  render(
    <Wishlist
      wishlist={wishlist}
      removeFromWishlist={removeFromWishlist}
      clearWishlist={clearWishlist}
    />
  );
  const emptyWishlistMessage = screen.getByText("Wishlist is empty");
  expect(emptyWishlistMessage).toBeInTheDocument();
});

test("renders wishlist items when wishlist is not empty", () => {
  const wishlist = [
    {
      id: 1,
      title: "Trip 1",
      description: "Description 1",
      price: 100,
      startTrip: new Date(),
      endTrip: new Date(),
    },
  ];
  const removeFromWishlist = jest.fn();
  const clearWishlist = jest.fn();
  render(
    <Wishlist
      wishlist={wishlist}
      removeFromWishlist={removeFromWishlist}
      clearWishlist={clearWishlist}
    />
  );
  const wishlistItem = screen.getByText("Trip 1");
  expect(wishlistItem).toBeInTheDocument();
});

test("calls clearWishlist function when 'empty wishlist' button is clicked", () => {
  const wishlist = [
    {
      id: 1,
      title: "Trip 1",
      description: "Description 1",
      price: 100,
      startTrip: new Date(),
      endTrip: new Date(),
    },
  ];
  const removeFromWishlist = jest.fn();
  const clearWishlist = jest.fn();
  render(
    <Wishlist
      wishlist={wishlist}
      removeFromWishlist={removeFromWishlist}
      clearWishlist={clearWishlist}
    />
  );
  const emptyWishlistButton = screen.getByText("empty wishlist");
  fireEvent.click(emptyWishlistButton);
  expect(clearWishlist).toHaveBeenCalled();
});