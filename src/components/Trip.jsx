export default function Trip({ addToWishlist, ...props}) {
   // Props
  let { trip,  "data-testid": dataTestId } = props;
  let { title, description } = trip;


  return (
    <div className="col-sm-6 col-md-4 col-lg-3" data-testid={dataTestId}>
      <figure className="card card-product">

        <div className="img-wrap">
          <img src={require(`../../public/images/items/${trip.id}.jpg`) /*fixed image import*/} alt="name " /> 
        </div>
        <figcaption className="info-wrap">
          {/*changed appearance of the trip to be readable */}
          <h5 className="title">
              {title}
          </h5>
          <h6>
            Starts: {new Date(...trip.startTrip).toLocaleString()}
          </h6>
          <h6>
            Ends: {new Date(...trip.endTrip).toLocaleString()}
          </h6>

          <p className="card-text">{description}</p>
          <div className="info-wrap row">
            <button
              type="button"
              className="btn btn-link btn-outline"
              onClick={() => addToWishlist(trip)}
            >
              <i className="fa fa-shopping-cart" /> Add to Wishlist
            </button>
          </div>
        </figcaption>
      </figure>
    </div>
  );
}