export default function WishlistItem(props) {
    // deconstruct props
    const { removeFromWishlist, item } = props;
    // props
    let { id, title, description, price ,startTrip, endTrip } = item;
  
    console.log("WishlistItem", props);
  
    console.log("WishlistItem", item);
    return (
      <tr key={id}>
        <td>
          <figure className="media">
  
            <div className="img-wrap">
              <img
                className="img-thumbnail img-xs"
                src={"/images/items/" + id + ".jpg"}
                alt="img"
              />
            </div>
            <figcaption className="media-body">
              <h6 className="h6">{title}</h6>
              <dl className="dlist-inline small">
                <dd>{description}</dd>
              </dl>
              <dl className="dlist-inline small">
                <dt>{new Date(startTrip).toLocaleString()}</dt>
                <dd>{new Date(endTrip).toLocaleString()}</dd>
              </dl>
            </figcaption>
          </figure>
        </td>
        <td className="price-wrap price">
            <p>{price}</p>
        </td>
        
        <td className="text-right">
          <button
            className="btn btn-outline-danger"
            //onClick={ () => removeFromWishlist(props.item) } // App deleteItem
  
  
            onClick={() => removeFromWishlist(item)}
          >
            delete Item
          </button>
        </td>
      </tr>
    );
  }