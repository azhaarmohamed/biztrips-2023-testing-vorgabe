import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Trip from "./Trip";


// functional component ProductList, deconstruct props!
function TripList({ addToWishlist }) {
  const [month, setMonth] = useState("");
  const [trips, setTrips] = useState([]); // useState mit leerem Array initialisieren
  const months = ["Idle", "Jan", "Feb", "March", "April", "Mai", "June", "July", "August", "September", "October", "November", "December"];

  useEffect(() => {
    fetch('http://localhost:3001/trips') // Anpassung des Endpunkts
      .then(response => response.json())
      .then(data => setTrips(data));
  }, []);

  const empty = (
    <section>
      <p className="alert alert-info">Productlist is empty</p>
    </section>
  );

  // if month selected then filter the trips from month === month
  const filteredTrips = month
  ? trips.filter((trip) => {
    const tripStart = new Date(trip.startTrip[0], trip.startTrip[1], trip.startTrip[2]);
    const tripMonth = tripStart.getMonth() + 1;
    return tripMonth === parseInt(month);
  })
  : trips;

  return (
    <div className="container">
      <section>
        <h2 className="h4">Triplist-Catalog</h2>
         <section id="filters">
            <label htmlFor="month">Filter by Month:</label>
            <select
              id="month"
              value={month} // controlled component
              onChange={(e) => {
                //debugger;
                setMonth(e.target.value);
              }}
            >
              <option value="">All Months</option>
              <option value="1">January</option>
              <option value="2">February</option>
              <option value="3">March</option>
              <option value="4">April</option>
              <option value="5">Mai</option>
              <option value="6">June</option>
              <option value="7">July</option>
              <option value="8">August</option>
              <option value="9">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
            {month && (
            <h2>
              Found {filteredTrips.length} 
              {filteredTrips.length === 1 ? " trip" : " trips" /* changed trips to trip */} for the month of
              {" " + months[month]}
            </h2>
          )}
          </section>
        <div className="row">
          {filteredTrips.length > 0 ? (
              filteredTrips.map((trip, index) => (
                <Trip addToWishlist={addToWishlist} trip={trip} key={trip.id} data-testid="trip" />
              ))
            ) : (
              empty
            )}
        </div>
      </section>
    </div>
  );
}
// deconstruct ...props


export default TripList;
