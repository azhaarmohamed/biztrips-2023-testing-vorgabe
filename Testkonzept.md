# Testkonzept für React-Komponenten

Für die Tests verwende ich die @testing-library/react.

## 1. Testen der Trip-Komponente:

### 1.1 Funktionalität

- Überprüfe, ob die `Trip`-Komponente korrekt gerendert wird.
- Stelle sicher, dass die übergebenen Trip-Daten korrekt in der Komponente angezeigt werden.
- Teste die korrekte Funktion des "Add to Wishlist"-Buttons.

### 1.2 Darstellung

- Überprüfe das Erscheinungsbild der Trip-Karte.
- Teste, ob das Bild korrekt gerendert wird.
- Stelle sicher, dass Titel, Start- und Enddatum sowie Beschreibung korrekt angezeigt werden.

### 1.3 Randfälle

- Teste die `Trip`-Komponente mit leeren Trip-Daten.
- Überprüfe, wie die Komponente auf fehlende Bilder oder ungültige Daten reagiert.

## 2. Testen der TripList-Komponente:

### 2.1 Datenabruf

- Überprüfe, ob die `TripList`-Komponente die Daten erfolgreich vom Server abruft.
- Teste, wie die Komponente auf Fehler beim Datenabruf reagiert.

### 2.2 Filterung

- Teste die Filterung nach Monat. Überprüfe, ob die Anzeige der Trips basierend auf dem ausgewählten Monat korrekt erfolgt.
- Überprüfe, ob die Anzahl der angezeigten Trips korrekt ist.
- Teste das Verhalten der `TripList`, wenn keine Trips vorhanden sind.

### 2.3 Interaktionen

- Überprüfe, ob das Hinzufügen von Trips zur Wishlist korrekt funktioniert.
- Teste, wie die `TripList` auf Änderungen im Monatsfilter reagiert.

## 3. Testen der WishlistItem-Komponente:

### 3.1 Darstellung

- Überprüfe, ob die `WishlistItem`-Komponente korrekt gerendert wird.
- Stelle sicher, dass Titel, Start- und Enddatum sowie Beschreibung korrekt angezeigt werden.
- Teste die korrekte Anzeige des Preises.

### 3.2 Aktionen

- Teste die Funktionalität des "Delete Item"-Buttons und ob er das richtige Item aus der Wishlist entfernt.
- Überprüfe, wie die `WishlistItem`-Komponente auf ungültige Daten oder fehlende Bilder reagiert.

## 4. Testen der Wishlist-Komponente:

### 4.1 Darstellung

- Überprüfe, ob die `Wishlist`-Komponente korrekt gerendert wird.
- Teste, ob die Wishlist korrekt angezeigt wird, wenn Elemente vorhanden sind, und die leere Nachricht, wenn die Wishlist leer ist.

### 4.2 Aktionen

- Teste die Funktionalität des "Empty Wishlist"-Buttons und ob er alle Elemente aus der Wishlist entfernt.
- Überprüfe, wie die `Wishlist`-Komponente auf leere oder ungültige Daten in der Wishlist reagiert.

## 5. Integrationstests:

- Teste die Zusammenarbeit zwischen `Trip`, `TripList`, `WishlistItem` und `Wishlist`.
- Stelle sicher, dass das Hinzufügen von Trips zur Wishlist und das Entfernen von Items korrekt funktionieren.
- Überprüfe, wie die Komponenten auf unerwartete Änderungen in den Datenstrukturen reagieren.

## 6. Usability-Tests:

- Überprüfe die Benutzerfreundlichkeit der Filteroptionen in der `TripList`.
- Teste, ob die Wishlist-Aktionen für den Benutzer intuitiv sind.
- Stelle sicher, dass die Ladezeiten bei verschiedenen Datensätzen akzeptabel sind.

## 7. Fehlermanagement:

- Teste, wie die Komponenten auf unerwartete Daten oder Benutzeraktionen reagieren.
- Überprüfe, ob Fehlermeldungen klar und verständlich für den Benutzer sind.

## 8. Risikomanagement:

- Identifiziere mögliche Risiken, wie z.B. unerwartete API-Änderungen, und plane entsprechende Testszenarien.
- Teste, wie die Anwendung auf Netzwerkprobleme oder Serverausfälle reagiert.

## 9. Backend-Tests mit Postman:

- Überprüfe die Funktionalität des JSON-Backends auf localhost:3001/trips.
- Teste die HTTP-Methoden POST, GET und DELETE mit Postman, um sicherzustellen, dass die Backend-Endpunkte korrekt funktionieren.
- Stelle sicher, dass die Anwendung angemessen auf Backend-Fehler reagiert.

Die Implementierung der Tests erfolgt unter Verwendung des Jest-Frameworks für React-Komponenten und Postman für Backend-Tests. Jest bietet eine umfassende und effektive Möglichkeit, Unit-Tests sowie Integrations- und Snapshot-Tests in einer React-Anwendung durchzuführen. Postman ermöglicht die Überprüfung der Backend-Endpunkte und die Gewährleistung der korrekten Interaktion zwischen Frontend und Backend.

Falls weitere Anpassungen oder spezifische Informationen erforderlich sind, können diese entsprechend den Anforderungen des Projekts hinzugefügt werden.


