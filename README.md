# Biztrips-mohamed - Kurze Projektbeschreibung

Biztrips-mohamed ist eine React-Anwendung, die dazu dient, Reisen anzuzeigen und in eine Wunschliste aufzunehmen. Die Anwendung verwendet React-Komponenten, um eine benutzerfreundliche Oberfläche bereitzustellen.

## Installation

1. Klone das Repository: `git clone https://gitlab.com/azhaarmohamed/biztrips-2023-testing-vorgabe.git`
2. Navigiere in das Projektverzeichnis: `cd biztrips-2023-testing-vorgabe`
3. Installiere die Abhängigkeiten: `npm install`

## Verwendung

- Starte die Anwendung: `npm start`
- Öffne [http://localhost:3000](http://localhost:3000) in deinem Browser.

## Testen

Für die Durchführung von Tests verwenden wir das Jest-Framework für React-Komponenten und Postman für Backend-Tests. Das vollständige Testkonzept findest du in der [Testkonzept.md](./Testkonzept.md)-Datei.

## Backend

Die Anwendung setzt voraus, dass ein JSON-Backend auf [http://localhost:3001/trips](http://localhost:3001/trips) verfügbar ist. Die Backend-Endpunkte werden ebenfalls mit Postman getestet.

## Mitwirkende

- Azhaar Mohamed Schülerin der Berufsbildungsschule Winterthur

---

Kurze und bündige Informationen, die die Installation, Verwendung, Testen und Backend-Anforderungen hervorheben. Benutzer können auf das ausführliche Testkonzept in der
[Testkonzept.md](./Testkonzept.md)-Datei verweisen, um weitere Informationen zu den Tests zu erhalten. Die Postman Test findet man hier [PostmanTest](TESTS_Biztrips.postman_collection)
